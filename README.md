**Points that I have covered:**

    - All points from basic features.
    - Point no. 2 (either by time posted) from bonus features.
    - Point no. 3 from bonus features.

**Libraries that I have used:**    

    - Intl
    - share_plus
    - auto_size_text
    - flutter_easyloading
    - cloud_firestore
    - firebase_auth
    - firebase_core

**Firestore Database Structure:**

    {
        uId: value (string)
        name: value (string)
        joke: value (string)
        timestamp: value (timestamp)
        likes: value (int)
        dislikes: value (int)
        like_users: {
            $user_id: value (boolean)
            ...
        }
        dislike_users: {
            $user_id: value (boolean)
            ...
        }
        starred_users: {
            $user_id: value (boolean)
            $user_id: value (boolean)
            ...
        }
    }
    
