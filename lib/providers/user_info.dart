import 'package:firebase_auth/firebase_auth.dart';

class CurrentUserInfo {
  //static final uName = FirebaseAuth.instance.currentUser?.displayName;
  static final uEmail = FirebaseAuth.instance.currentUser?.email;

  static String get greetings {
    var hour = DateTime.now().hour;
    if (hour < 12) {
      return 'Good Morning';
    }
    if (hour < 17) {
      return 'Good Afternoon';
    }
    return 'Good Evening';
  }

  static Future<String> userFirstName(String uName) async {
    return uName.split(' ')[0];
  }

  static Future<String> userAbbreviatedName(String uName) async {
    var x = uName.split(' ');
    if (x.length < 2) {
      return x[0].split('')[0].toUpperCase();
    } else {
      return (x[0].split('')[0] + x[1].split('')[0]).toUpperCase();
    }
  }
}
