import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jokes_app/app/app_router.dart';
import 'package:jokes_app/providers/user_info.dart';

class HomeDrawer extends StatelessWidget {
  const HomeDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          DrawerHeader(
            child: FutureBuilder(
                future: FirebaseFirestore.instance
                    .collection('users')
                    .doc(FirebaseAuth.instance.currentUser?.uid)
                    .get(),
                builder: (BuildContext context,
                    AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>>
                        snapshot) {
                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container();
                  }
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CircleAvatar(
                        radius: 35,
                        backgroundColor: Theme.of(context).primaryColor,
                        child: Text(
                          snapshot.data!['abb_name'],
                          style: const TextStyle(
                            fontSize: 27,
                            color: Colors.white,
                            fontFamily: 'Quicksand',
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      const SizedBox(height: 10),
                      Text(
                        snapshot.data!['first_name'],
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Quicksand',
                          fontSize: 16,
                        ),
                        maxLines: 1,
                      ),
                      const SizedBox(height: 5),
                      Text(
                        CurrentUserInfo.uEmail!,
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Quicksand',
                          fontSize: 16,
                        ),
                        maxLines: 1,
                      ),
                    ],
                  );
                }),
          ),
          ListTile(
            title: const Text(
              'My Jokes',
              style: TextStyle(fontFamily: 'OpenSans'),
            ),
            leading: const Icon(
              Icons.face,
              color: Colors.redAccent,
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed(AppRouter.myJokesRoute);
            },
          ),
          const Divider(),
          ListTile(
            title: const Text(
              'Starred Jokes',
              style: TextStyle(fontFamily: 'OpenSans'),
            ),
            leading: const Icon(
              Icons.star,
              color: Colors.amber,
            ),
            onTap: () {
              Navigator.of(context).pop();
              Navigator.of(context).pushNamed(AppRouter.starredRoute);
            },
          ),
          const Divider(),
          ListTile(
            title: const Text(
              'Logout',
              style: TextStyle(fontFamily: 'OpenSans'),
            ),
            leading: const Icon(
              Icons.logout,
              color: Colors.black87,
            ),
            onTap: () {
              FirebaseAuth.instance.signOut().then(
                (_) {
                  Navigator.of(context).pushNamedAndRemoveUntil(
                      AppRouter.loginRoute, (route) => false);
                },
              );
            },
          ),
        ],
      ),
    );
  }
}
