import 'dart:ui';

import 'package:flutter/material.dart';

class BottomNavigation extends StatefulWidget {
  const BottomNavigation({
    Key? key,
    required this.selectedIndex,
    required this.setCurrentIndex,
  }) : super(key: key);

  final int selectedIndex;
  final void Function(int index) setCurrentIndex;

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation> {
  @override
  Widget build(BuildContext context) {
    return NavigationBarTheme(
      data: NavigationBarThemeData(
        labelTextStyle: MaterialStateProperty.all(
          const TextStyle(
            fontSize: 14,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      child: NavigationBar(
        height: 65,
        onDestinationSelected: (index) {
          widget.setCurrentIndex(index);
        },
        selectedIndex: widget.selectedIndex,
        destinations: const [
          NavigationDestination(icon: Icon(Icons.home), label: 'Home'),
          NavigationDestination(icon: Icon(Icons.post_add), label: 'Post')
        ],
      ),
    );
  }
}
