import 'package:flutter/material.dart';

class FormSubmitButton extends StatelessWidget {
  final String lable;
  final void Function(BuildContext context) onSubmitMethodRef;

  const FormSubmitButton({
    required this.lable,
    required this.onSubmitMethodRef,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      child: Text(
        lable,
        style: const TextStyle(fontWeight: FontWeight.bold),
      ),
      onPressed: () => onSubmitMethodRef(context),
    );
  }
}
