import 'package:flutter/material.dart';
import 'package:jokes_app/app/app_colors.dart';

import 'form_input_decoration.dart';

class UserForm extends StatefulWidget {
  final String? Function(String? val) validatorMethodRef;
  final void Function(String? val) onSavedMethodRef;

  final String lableText;
  final bool obscureText;
  final TextInputType keyboardType;

  const UserForm({
    required this.validatorMethodRef,
    required this.onSavedMethodRef,
    required this.lableText,
    required this.obscureText,
    required this.keyboardType,
    Key? key,
  }) : super(key: key);

  @override
  _UserFormState createState() => _UserFormState();
}

class _UserFormState extends State<UserForm> {
  late FocusNode _focusNode;
  late TextEditingController _controller;

  @override
  void initState() {
    super.initState();
    _focusNode = FocusNode();
    _focusNode.addListener(() => setState(() {}));
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    super.dispose();
    _focusNode.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      autofillHints: null,
      focusNode: _focusNode,
      controller: _controller,
      obscureText: widget.obscureText,
      keyboardType: widget.keyboardType,
      textInputAction: TextInputAction.next,
      cursorColor: AppColors.primaryColor,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      style: Theme.of(context).textTheme.headline6,
      decoration: FormInputDecoration(
        context: context,
        focusNode: _focusNode,
        labelText: widget.lableText,
      ).inputDecoration(),
      validator: widget.validatorMethodRef,
      onSaved: widget.onSavedMethodRef,
    );
  }
}
