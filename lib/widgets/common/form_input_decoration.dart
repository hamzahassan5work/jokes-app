import 'package:flutter/material.dart';

import '/app/app_colors.dart';

class FormInputDecoration {
  final String labelText;
  final FocusNode focusNode;
  final Widget suffixIcon;
  final BuildContext context;

  const FormInputDecoration({
    required this.context,
    required this.focusNode,
    required this.labelText,
    this.suffixIcon = const Icon(null),
  });

  UnderlineInputBorder _underlineInputBorder() {
    return UnderlineInputBorder(
      borderSide: BorderSide(
        width: 2.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  InputDecoration inputDecoration() {
    return InputDecoration(
      labelText: labelText,
      labelStyle: TextStyle(
        color: focusNode.hasFocus
            ? Theme.of(context).primaryColor
            : AppColors.unFocusTextFieldLabelColor,
        fontSize: 14,
      ),
      filled: true,
      fillColor: AppColors.textFieldFillColor,
      errorBorder: _underlineInputBorder(),
      focusedErrorBorder: _underlineInputBorder(),
      focusedBorder: _underlineInputBorder(),
      suffixIcon: suffixIcon,
    );
  }
}
