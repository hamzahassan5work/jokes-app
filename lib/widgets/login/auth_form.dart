import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:jokes_app/app/app_router.dart';
import 'package:jokes_app/firebase/auth/auth_service.dart';
import 'package:jokes_app/widgets/common/form_field.dart';
import 'package:jokes_app/widgets/common/form_submit_button.dart';

// ignore: must_be_immutable
class AuthForm extends StatelessWidget {
  AuthForm({
    Key? key,
    required this.isLogin,
    required this.changeAuthState,
  }) : super(key: key);

  final bool isLogin;
  final VoidCallback changeAuthState;

  final _authFormKey = GlobalKey<FormState>();

  String? _userName;
  String? _userEmail;
  String? _userPassword;

  String? nameValidator(String? name) {
    if (name!.trim().isEmpty) {
      return 'Enter Full Name';
    }
    return null;
  }

  String? emailValidator(String? email) {
    if (email!.trim().isEmpty) {
      return 'Enter Email';
    }
    return null;
  }

  String? passwordValidator(String? password) {
    if (password!.trim().isEmpty) {
      return 'Enter Password';
    }
    return null;
  }

  void nameOnSaved(String? name) {
    _userName = name!.trim();
  }

  void emailOnSaved(String? email) {
    _userEmail = email!.trim();
  }

  void passwordOnSaved(String? password) {
    _userPassword = password!.trim();
  }

  void onLogin(BuildContext context) async {
    FocusScope.of(context).unfocus();
    if (_authFormKey.currentState!.validate()) {
      _authFormKey.currentState!.save();
      EasyLoading.show(status: "Logging in, please wait");
      var rslt = await AuthService.login(_userEmail!, _userPassword!);
      if (rslt == 'Login Successful') {
        EasyLoading.dismiss();
        Navigator.of(context).pushNamedAndRemoveUntil(
          AppRouter.homeRoute,
          (route) => false,
        );
      } else {
        EasyLoading.showError(rslt);
      }
    }
  }

  void onSignup(BuildContext context) async {
    FocusScope.of(context).unfocus();
    if (_authFormKey.currentState!.validate()) {
      _authFormKey.currentState!.save();
      EasyLoading.show(status: 'Creating an account, please wait');
      var rslt = await AuthService.createUser(
        _userEmail!,
        _userPassword!,
        _userName!,
      );
      if (rslt == 'Created Successfully') {
        EasyLoading.dismiss();
        Navigator.of(context).pushNamedAndRemoveUntil(
          AppRouter.homeRoute,
          (route) => false,
        );
      } else {
        EasyLoading.showError(rslt);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _authFormKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          if (!isLogin) ...[
            UserForm(
              key: UniqueKey(),
              validatorMethodRef: nameValidator,
              onSavedMethodRef: nameOnSaved,
              lableText: 'Full Name',
              obscureText: false,
              keyboardType: TextInputType.name,
            ),
            const SizedBox(height: 20),
          ],
          UserForm(
            key: UniqueKey(),
            validatorMethodRef: emailValidator,
            onSavedMethodRef: emailOnSaved,
            lableText: 'Email',
            obscureText: false,
            keyboardType: TextInputType.emailAddress,
          ),
          const SizedBox(height: 20),
          UserForm(
            key: UniqueKey(),
            validatorMethodRef: passwordValidator,
            onSavedMethodRef: passwordOnSaved,
            lableText: 'Password',
            obscureText: true,
            keyboardType: TextInputType.visiblePassword,
          ),
          const SizedBox(height: 40),
          FormSubmitButton(
            lable: isLogin ? 'LOGIN' : 'SIGN UP',
            onSubmitMethodRef: isLogin ? onLogin : onSignup,
          ),
          const SizedBox(height: 30),
          TextButton(
            onPressed: changeAuthState,
            child: isLogin
                ? const Text("Don't have an account yet?")
                : const Text("Already have an account?"),
          )
        ],
      ),
    );
  }
}
