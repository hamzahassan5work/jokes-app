import 'package:flutter/material.dart';

class AuthHeading extends StatelessWidget {
  const AuthHeading({
    Key? key,
    required this.isLogin,
  }) : super(key: key);

  final bool isLogin;

  Text textBuilder(String text, double fontSize, FontWeight fontWeight,
      [Color fontColor = Colors.black87]) {
    return Text(
      text,
      style: TextStyle(
        fontSize: fontSize,
        fontWeight: fontWeight,
        color: fontColor,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Text>[
          textBuilder(
            isLogin ? 'Login' : 'Register',
            20,
            FontWeight.bold,
          ),
          textBuilder(
            isLogin ? 'Login to continue' : 'Create an account',
            15,
            FontWeight.normal,
            Colors.black.withOpacity(0.3),
          ),
        ],
      ),
    );
  }
}
