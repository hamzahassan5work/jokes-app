class AppConstants {
  static const appName = 'Jokes App';
  static const appTagline = 'Put on a happy face';
}
