import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color.fromARGB(255, 123, 104, 238);
  static const Color canvasColor = Color.fromARGB(255, 249, 249, 249);
  static const Color textFieldTextColor = Color.fromARGB(222, 0, 0, 0);
  static const Color textFieldFillColor = Colors.white;
  static const Color unFocusTextFieldLabelColor = Color.fromARGB(153, 0, 0, 0);
  static const Color unFocusTextFieldSuffixIconColor =
      Color.fromARGB(153, 0, 0, 0);
  static const Color appBarTextColor = Colors.black;
  static const Color appBarIconColor = Colors.black;
  static const Color appBarActionsIconColor =
      Color.fromARGB(255, 102, 102, 102);
  static const Color appBarBackgroundColor = Colors.white;
  static const Color tileColorWhenTimerRunning = primaryColor;
  static const Color tileColorWhenEndTimeMissing = Colors.orange;
  static const Color tileColorWhenCompletePair = Colors.green;
}
