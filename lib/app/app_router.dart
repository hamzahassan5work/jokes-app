import 'package:flutter/material.dart';
import 'package:jokes_app/screens/login/login_screen.dart';
import 'package:jokes_app/screens/main/main_screen.dart';
import 'package:jokes_app/screens/my%20jokes/my_jokes_screen.dart';
import 'package:jokes_app/screens/splash/splash_screen.dart';
import 'package:jokes_app/screens/starred/starred_screen.dart';

class AppRouter {
  static const splashRoute = '/splash';
  static const loginRoute = '/login';
  static const homeRoute = '/home';
  static const starredRoute = '/starred';
  static const myJokesRoute = '/my_jokes';
  

  static MaterialPageRoute onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case splashRoute:
        return MaterialPageRoute(
          builder: (_) => const SplashScreen(),
        );
      case loginRoute:
        return MaterialPageRoute(
          builder: (_) => const LoginScreen(),
        );
      case homeRoute:
        return MaterialPageRoute(
          builder: (_) => const MainScreen(),
        );
      case starredRoute:
        return MaterialPageRoute(
          builder: (_) => const StarredScreen(),
        );
      case myJokesRoute:
        return MaterialPageRoute(
          builder: (_) => const MyJokesScreen(),
        );
      default:
        return MaterialPageRoute(
          builder: (_) => const MainScreen(),
        );
    }
  }
}
