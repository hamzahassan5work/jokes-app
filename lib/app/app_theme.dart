import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_colors.dart';

class AppTheme {
  static ThemeData themeData() {
    return ThemeData(
      fontFamily: 'Quicksand',
      primaryColor: AppColors.primaryColor,
      canvasColor: AppColors.canvasColor,
      textTheme: ThemeData.light().textTheme.copyWith(
            headline6: const TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.normal,
              color: AppColors.textFieldTextColor,
            ),
          ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith(
            (states) => AppColors.primaryColor,
          ),
        ),
      ),
      appBarTheme: const AppBarTheme(
        elevation: 1,
        centerTitle: false,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        backgroundColor: AppColors.appBarBackgroundColor,
        iconTheme: IconThemeData(
          color: AppColors.appBarIconColor,
        ),
        actionsIconTheme: IconThemeData(
          color: AppColors.appBarActionsIconColor,
        ),
        titleTextStyle: TextStyle(
          fontSize: 20,
          color: AppColors.appBarTextColor,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }
}
