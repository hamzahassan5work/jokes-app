import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:jokes_app/providers/user_info.dart';

class AuthService {
  static final _auth = FirebaseAuth.instance;

  static Stream<User?> get userAuthState => _auth.authStateChanges();

  static Future<String> createUser(
      String email, String password, String userName) async {
    String status = '';
    try {
      await _auth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      var firstName = await CurrentUserInfo.userFirstName(userName);
      var abbreviatedName = await CurrentUserInfo.userAbbreviatedName(userName);
      await FirebaseFirestore.instance
          .collection('users')
          .doc(FirebaseAuth.instance.currentUser?.uid)
          .set({
        'name': userName,
        'first_name': firstName,
        'abb_name': abbreviatedName,
      });
      status = 'Created Successfully';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'email-already-in-use') {
        status = 'The account already exists for that email';
      } else if (e.code == 'invalid-email') {
        status = 'Email address is not valid';
      } else if (e.code == 'weak-password') {
        status = 'The password provided is too weak';
      }
    }
    return status;
  }

  static Future<String> login(String email, String password) async {
    var status = '';
    try {
      await _auth.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      status = 'Login Successful';
    } on FirebaseAuthException catch (e) {
      if (e.code == 'invalid-email' ||
          e.code == 'wrong-password' ||
          e.code == 'user-not-found') {
        status = 'Incorrect email and password';
      }
    }
    return status;
  }
}
