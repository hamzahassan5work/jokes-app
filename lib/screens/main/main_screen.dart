import 'package:flutter/material.dart';
import 'package:jokes_app/screens/home/home_screen.dart';
import 'package:jokes_app/screens/post/post_screen.dart';
import 'package:jokes_app/widgets/home/bottom_navigation.dart';
import 'package:jokes_app/widgets/home/home_app_bar.dart';
import 'package:jokes_app/widgets/home/home_drawer.dart';

class MainScreen extends StatefulWidget {
  const MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 0;

  void setCurrentIndex(int currentIndex) {
    setState(() {
      _currentIndex = currentIndex;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const HomeAppBar(),
      drawer: const HomeDrawer(),
      bottomNavigationBar: BottomNavigation(
        selectedIndex: _currentIndex,
        setCurrentIndex: setCurrentIndex,
      ),
      body: _currentIndex == 0 ? HomeScreen() : PostScreen(),
    );
  }
}
