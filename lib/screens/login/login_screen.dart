import 'package:flutter/material.dart';
import 'package:jokes_app/widgets/common/app_logo.dart';
import 'package:jokes_app/widgets/login/auth_form.dart';
import 'package:jokes_app/widgets/login/auth_heading.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: _BuildBody(),
    );
  }
}

class _BuildBody extends StatefulWidget {
  const _BuildBody({Key? key}) : super(key: key);

  @override
  State<_BuildBody> createState() => _BuildBodyState();
}

class _BuildBodyState extends State<_BuildBody> {
  bool _isLogin = true;

  void _changeAuthState() {
    setState(() {
      _isLogin = !_isLogin;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
        top: 60.0,
        bottom: 30.0,
        left: 30.0,
        right: 30.0,
      ),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const AppLogo(),
            const SizedBox(height: 50),
            AuthHeading(isLogin: _isLogin),
            const SizedBox(height: 50),
            AuthForm(
              isLogin: _isLogin,
              changeAuthState: _changeAuthState,
            ),
          ],
        ),
      ),
    );
  }
}
