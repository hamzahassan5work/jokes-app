import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class StarredScreen extends StatelessWidget {
  const StarredScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Starred Jokes'),
      ),
      body: StreamBuilder(
        stream: FirebaseFirestore.instance
            .collection('jokes')
            .where('starred.${FirebaseAuth.instance.currentUser?.uid}',
                isEqualTo: true)
            .snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.hasError) {
            return const Center(child: Text('Something went wrong'));
          }

          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }

          if (snapshot.data!.docs.isEmpty) {
            return const Center(
              child: Text('No Jokes'),
            );
          }

          return ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;

            final DateTime dt = (data['timestamp'] as Timestamp).toDate();
            final String date = DateFormat('MM/dd/yyyy, hh:mm a').format(dt);
            final String joke = data['joke'];
            final int likes = data['likes'];
            final int dislikes = data['dislikes'];

            return Card(
              margin: const EdgeInsets.all(8.0),
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    ListTile(
                      trailing: IconButton(
                        icon: const Icon(
                          Icons.star,
                          color: Colors.blue,
                        ),
                        onPressed: () {
                          FirebaseFirestore.instance
                              .collection('jokes')
                              .doc(document.id)
                              .update({
                            'starred.${FirebaseAuth.instance.currentUser?.uid}':
                                false,
                          });
                        },
                      ),
                      minVerticalPadding: 15,
                      title: Text(
                        joke,
                        style: const TextStyle(fontFamily: 'OpenSans'),
                      ),
                      subtitle: Text(
                        date,
                        style: const TextStyle(fontFamily: 'Quicksand'),
                      ),
                    ),
                    const Divider(),
                    const SizedBox(height: 8),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(
                          'Likes: $likes',
                          style: const TextStyle(fontFamily: 'OpenSans'),
                        ),
                        Text(
                          'Dislikes: $dislikes',
                          style: const TextStyle(fontFamily: 'OpenSans'),
                        )
                      ],
                    )
                  ],
                ),
              ),
            );
          }).toList());
        },
      ),
    );
  }
}
