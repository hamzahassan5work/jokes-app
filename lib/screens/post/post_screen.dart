import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:jokes_app/app/app_colors.dart';

// ignore: must_be_immutable
class PostScreen extends StatelessWidget {
  PostScreen({Key? key}) : super(key: key);

  UnderlineInputBorder _underlineInputBorder(BuildContext context) {
    return UnderlineInputBorder(
      borderSide: BorderSide(
        width: 2.0,
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  final controller = TextEditingController();

  CollectionReference jokes = FirebaseFirestore.instance.collection('jokes');

  Future<void> addJoke() async {
    var doc = await FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser?.uid)
        .get();
    String uName = doc.data()?['name'];
    return jokes
        .add({
          'uId': FirebaseAuth.instance.currentUser?.uid,
          'name': uName,
          'timestamp': Timestamp.now(),
          'joke': controller.text,
          'likes': 0,
          'dislikes': 0,
          'starred': {},
          'like_users': {},
          'dislike_users': {},
        })
        .then((_) => controller.clear())
        // ignore: avoid_print
        .catchError((error) => print("Failed to add joke: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            TextField(
              maxLength: 300,
              controller: controller,
              textInputAction: TextInputAction.done,
              maxLines: null,
              style: const TextStyle(fontFamily: 'OpenSans'),
              decoration: InputDecoration(
                label: const Text('Enter Your Joke'),
                filled: true,
                fillColor: Colors.white,
                focusedBorder: _underlineInputBorder(context),
                labelStyle: const TextStyle(
                  color: AppColors.primaryColor,
                  fontSize: 17,
                  fontFamily: 'OpenSans',
                ),
              ),
            ),
            const SizedBox(height: 20),
            ElevatedButton(
              onPressed: addJoke,
              child: const Text('SUBMIT JOKE'),
            ),
          ],
        ),
      ),
    );
  }
}
