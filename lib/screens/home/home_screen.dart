import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:share_plus/share_plus.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool isliked = false;
  bool isdisliked = false;
  bool isStarred = false;

  String userAbbreviatedName(String name) {
    var x = name.split(' ');
    if (x.length < 2) {
      return x[0].split('')[0].toUpperCase();
    } else {
      return (x[0].split('')[0] + x[1].split('')[0]).toUpperCase();
    }
  }

  void handleLikeJoke(String docId, int totalLikes, Map? likeUsers) async {
    bool isLiked;
    var snapshot =
        await FirebaseFirestore.instance.collection('jokes').doc(docId).get();
    Map listlikeUsers = snapshot.data()!['like_users'];

    if (listlikeUsers.containsKey(FirebaseAuth.instance.currentUser?.uid)) {
      isLiked = listlikeUsers[FirebaseAuth.instance.currentUser?.uid] as bool;
    } else {
      isLiked = false;
    }

    if (!isLiked) {
      likeUsers![FirebaseAuth.instance.currentUser!.uid] = true;
      FirebaseFirestore.instance.collection('jokes').doc(docId).update({
        'like_users': likeUsers,
        'likes': totalLikes += 1,
      });
    } else {
      likeUsers![FirebaseAuth.instance.currentUser!.uid] = false;
      FirebaseFirestore.instance.collection('jokes').doc(docId).update({
        'like_users': likeUsers,
        'likes': totalLikes -= 1,
      });
    }
  }

  void handleDislikeJoke(
      String docId, int totalDislikes, Map? dislikeUsers) async {
    bool isDisliked;
    var snapshot =
        await FirebaseFirestore.instance.collection('jokes').doc(docId).get();
    Map listdislikeUsers = snapshot.data()!['dislike_users'];

    if (listdislikeUsers.containsKey(FirebaseAuth.instance.currentUser?.uid)) {
      isDisliked =
          listdislikeUsers[FirebaseAuth.instance.currentUser?.uid] as bool;
    } else {
      isDisliked = false;
    }

    if (!isDisliked) {
      dislikeUsers![FirebaseAuth.instance.currentUser!.uid] = true;
      FirebaseFirestore.instance.collection('jokes').doc(docId).update({
        'dislike_users': dislikeUsers,
        'dislikes': totalDislikes += 1,
      });
    } else {
      dislikeUsers![FirebaseAuth.instance.currentUser!.uid] = false;
      FirebaseFirestore.instance.collection('jokes').doc(docId).update({
        'dislike_users': dislikeUsers,
        'dislikes': totalDislikes -= 1,
      });
    }
  }

  void handleStarredJoke(String docId, Map? starredJokes) async {
    bool isJokeStarred;
    var snapshot =
        await FirebaseFirestore.instance.collection('jokes').doc(docId).get();
    Map listStarredJokes = snapshot.data()!['starred'];

    if (listStarredJokes.containsKey(FirebaseAuth.instance.currentUser?.uid)) {
      isJokeStarred =
          listStarredJokes[FirebaseAuth.instance.currentUser?.uid] as bool;
    } else {
      isJokeStarred = false;
    }

    if (!isJokeStarred) {
      starredJokes![FirebaseAuth.instance.currentUser!.uid] = true;
      FirebaseFirestore.instance
          .collection('jokes')
          .doc(docId)
          .update({'starred': starredJokes});
    } else {
      starredJokes![FirebaseAuth.instance.currentUser!.uid] = false;
      FirebaseFirestore.instance
          .collection('jokes')
          .doc(docId)
          .update({'starred': starredJokes});
    }
  }

  final Stream<QuerySnapshot> _usersStream = FirebaseFirestore.instance
      .collection('jokes')
      .orderBy('timestamp', descending: true)
      .snapshots();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _usersStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return const Center(child: Text('Something went wrong'));
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return const Center(child: CircularProgressIndicator());
        }

        if (snapshot.data!.docs.isEmpty) {
          return const Center(
            child: Text('No Jokes Available :('),
          );
        }

        return ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            var currentUserId = FirebaseAuth.instance.currentUser?.uid;

            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;

            final String name = data['name'];
            final String abbreviatedName = userAbbreviatedName(name);
            final DateTime dt = (data['timestamp'] as Timestamp).toDate();
            final String date = DateFormat('MM/dd/yyyy, hh:mm a').format(dt);
            final String joke = data['joke'];
            final int likes = data['likes'];
            final int dislikes = data['dislikes'];
            final Map? starred = data['starred'];
            final Map? likeUsers = data['like_users'];
            final Map? dislikeUsers = data['dislike_users'];

            if (likeUsers != null) {
              if (likeUsers.containsKey(currentUserId)) {
                isliked = likeUsers[currentUserId] as bool;
              } else {
                isliked = false;
              }
            }

            if (dislikeUsers != null) {
              if (dislikeUsers.containsKey(currentUserId)) {
                isdisliked = dislikeUsers[currentUserId] as bool;
              } else {
                isdisliked = false;
              }
            }

            if (starred != null) {
              if (starred.containsKey(currentUserId)) {
                isStarred = starred[currentUserId] as bool;
              } else {
                isStarred = false;
              }
            }

            return Card(
              margin: const EdgeInsets.all(8),
              child: Padding(
                padding: const EdgeInsets.only(top: 8, left: 8, right: 8),
                child: Column(
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          child: Text(abbreviatedName),
                        ),
                        const SizedBox(width: 10),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              name,
                              style: const TextStyle(fontFamily: 'Quicksand'),
                            ),
                            const SizedBox(height: 4),
                            Text(
                              date,
                              style: const TextStyle(fontFamily: 'Quicksand'),
                            ),
                          ],
                        )
                      ],
                    ),
                    const SizedBox(height: 8),
                    ListTile(
                        title: Text(
                      joke,
                      style: const TextStyle(fontFamily: 'OpenSans'),
                    )),
                    const Divider(),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        TextButton.icon(
                          onPressed: () =>
                              handleLikeJoke(document.id, likes, likeUsers),
                          icon: isliked
                              ? const Icon(Icons.thumb_up_alt)
                              : const Icon(Icons.thumb_up_alt_outlined),
                          label: Text('$likes'),
                        ),
                        TextButton.icon(
                          onPressed: () => handleDislikeJoke(
                              document.id, dislikes, dislikeUsers),
                          icon: isdisliked
                              ? const Icon(Icons.thumb_down_alt)
                              : const Icon(Icons.thumb_down_alt_outlined),
                          label: Text('$dislikes'),
                        ),
                        IconButton(
                          onPressed: () =>
                              handleStarredJoke(document.id, starred),
                          icon: isStarred
                              ? const Icon(
                                  Icons.star,
                                  color: Colors.blue,
                                )
                              : const Icon(Icons.star_border_rounded),
                        ),
                        IconButton(
                          onPressed: () {
                            Share.share(joke);
                          },
                          icon: const Icon(Icons.share_outlined),
                        ),
                        const SizedBox(height: 5),
                      ],
                    ),
                  ],
                ),
              ),
            );
          }).toList(),
        );
      },
    );
  }
}
